var defaultIntroTexts = {
    de: 'Willst auch du deine Posts hier sehen? Dann verwende folgenden Hashtag auf Twitter oder Instagram.',
    it: 'Anch tu vuoi vedere qui i tuoi post? Allora usa il seguente hashtag su Twitter o Instagram.',
    en: 'Do you want to see your posts here? Then use the following hashtag on Twitter or Instagram.'
};

db.getCollection('Wall').find({ 'intro': { $exists: false } }).forEach(function(wall){
    wall.intro = defaultIntroTexts;
    db.getCollection('Wall').save(wall);
});