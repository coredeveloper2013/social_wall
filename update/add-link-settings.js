db.SettingManager.update({
  "key": "show-author-link",
}, {
  "key": "show-author-link",
  "type": "boolean",
  "value": true,
  "roles": ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]
});

db.SettingManager.update({
  "key": "show-post-link",
}, {
  "key": "show-post-link",
  "type": "boolean",
  "value": true,
  "roles": ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]
});
