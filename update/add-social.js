db.SettingManager.update({
  "key" : "facebook-page",
}, {
  "key" : "facebook-page",
  "type" : "string",
  "roles": ["ROLE_ADMIN", "ROLE_SUPER_ADMIN"]
});
db.SettingManager.insert(
	{
		"key" : "facebook-redirect-url",
		"type" : "hidden"
	}
);
db.SettingManager.insert(
  {
    "key" : "facebook-access-token",
    "type" : "hidden"
  }
);
db.SettingManager.insert(
	{
		"key" : "facebook-name",
		"type" : "hidden"
	}
);
db.SettingManager.insert(
	{
		"key" : "facebook-picture",
		"type" : "hidden"
	}
);
db.SettingManager.insert(
  {
    "key" : "facebook-link",
    "type" : "hidden"
  }
);
