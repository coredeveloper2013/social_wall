<?php

namespace AppBundle\AWS;

use Aws\S3\S3Client;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DI\Service("aws.s3")
 */
class S3
{
    protected static $formats;

    /**
     * @DI\InjectParams({
     *	"container" = @DI\Inject("service_container"),
     * })
     */
    public function __construct(ContainerInterface $container)
    {
        $conf = array(
            'credentials' => array(
                'key' => $container->getParameter('aws.key'),
                'secret' => $container->getParameter('aws.secret'),
            ),
            'signature' => 'v4',
            'region' => 'eu-central-1',
            'version' => 'latest',
        );
        $this->bucket = $container->getParameter('aws.bucket');
        $this->s3 = S3Client::factory($conf);
        $this->keyPrefix = 'social-wall/';
    }

    public function saveContent($filename, $body, $invalidate = false)
    {
        $key = $this->keyPrefix.$filename;
        $this->s3->upload(
            $this->bucket,
            $key,
            $this->compress($body),
            'public-read',
            array(
                'params' => array('ContentType' => $this->getMimeType($filename), 'ContentEncoding' => 'gzip'),
            )
        );
    }

    public function exists($filename)
    {
        return $this->s3->doesObjectExist($this->bucket, $filename);
    }

    public function getUrl()
    {
        return 'https://'.$this->bucket.'.s3.amazonaws.com';
    }

    private function getMimeType($filename)
    {
        $format = pathinfo(basename($filename), PATHINFO_EXTENSION);
        if (null === static::$formats) {
            static::initializeFormats();
        }

        return isset(static::$formats[$format]) ? static::$formats[$format][0] : null;
    }

    protected static function initializeFormats()
    {
        static::$formats = array(
            'html' => array('text/html', 'application/xhtml+xml'),
            'txt' => array('text/plain'),
            'js' => array('application/javascript', 'application/x-javascript', 'text/javascript'),
            'css' => array('text/css'),
            'jpg' => array('image/jpeg'),
            'png' => array('image/png'),
        );
    }

    private function compress($body)
    {
        return "\x1f\x8b\x08\x00\x00\x00\x00\x00".substr(gzcompress($body, 9), 0, -4);
    }
}
