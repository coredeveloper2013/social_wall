<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as RestMethod;
use FOS\RestBundle\View\View as FOSView;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Trick17\ApiBundle\Controller\Annotations\Trick17ApiDoc;

class InfoController extends Controller
{
    /**
     * @ApiDoc
     * @Trick17ApiDoc
     * @RestMethod\Get("/info")
     */
    public function getAction(Request $request)
    {
        $settings = $this->get('settings');

        $data = [
            'show-author-link' => $settings->get('show-author-link'),
            'show-post-link' => $settings->get('show-post-link'),
        ];

        if (isset($facebookId)) {
            $data['facebookId'] = $facebookId;
        }

        return new FOSView($data);
    }
}
