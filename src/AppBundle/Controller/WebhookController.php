<?php

namespace AppBundle\Controller;

use AppBundle\Document\Wall;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Document\Style;

class WebhookController extends Controller
{
    /**
     * Get Preview.
     *
     * @Post("/{appInstance}")
     */
    public function postAction(Request $request)
    {
        if (!$request->request->has('event')) {
            return;
        }
        if ($request->request->get('event') == 'create') {
            $appInstance = $request->attributes->get('appInstance');
            $this->createStyle($appInstance);
            $this->createDefaultSocialWall($appInstance);
        }

        return;
    }

    private function createStyle($appInstance)
    {
        $dm = $this->get('doctrine_mongodb.odm.document_manager');
        $styleGenerator = $this->get('app_bundle.style.style_generator');

        $style = $dm->getRepository('AppBundle:Style')->findOneByAppInstance($appInstance);
        if ($style) {
            return;
        }
        $style = new Style();
        $style->setAppInstance($appInstance);

        $styleGenerator->generateWidgetStyle($style);

        $dm->persist($style);
        $dm->flush();
    }

    private function createDefaultSocialWall($appInstance)
    {
        $dm = $this->get('doctrine_mongodb.odm.document_manager');
        $modelHelper = $this->getModelHelper($appInstance);

        $settings = $this->get('settings');
        //lifetime accesstoken of "hotel an der Etsch/Toni huber", no expire-date
        $settings->set('facebook-access-token', 'EAAJukHDiZBHgBAKrFJpmtffvKMai07b93z2Rs4oRgv8dw39vZCfvnbnerhcfmZCICh6hZBIOrlzlf66FxkisJZAbhD2OkBQ9ZAZBiHvITn4ZA69M4I4VQZAVclOH7Kj1zYgvxGSIZATnTm6VAMNzOrWpZBKDtsCkh7jC6jHtCcpUybYIwZDZD');
        $settings->set('facebook-page', '747343658655755');
        $settings->set('facebook-link', 'Hotel an der Etsch');
        $settings->set('facebook-picture', 'https://graph.facebook.com/747343658655755/picture');

        $wall = new Wall();
        $wall->setAppInstance($appInstance);
        $wall->setCreatedAt(new \DateTime());
        $wall->setCreatedBy(0);
        $wall->setName('Ersteinrichtung SocialWall');
        $wall->setAllFacebookPosts(true);
        $wall->setMainHashtag('#t17ErsteinrichtungSocialWall');
        $dm->persist($wall);
        $modelHelper->dispatchCreateEvent($wall);

        $dm->flush();
    }

    private function getModelHelper($appInstance)
    {
        $modelHelper = $this->container->get('model.helper');
        $modelHelper->setAppInstance($appInstance);
        $modelHelper->setModelName('AppBundle\Document\Wall');

        return $modelHelper;
    }
}
