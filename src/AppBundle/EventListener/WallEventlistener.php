<?php

namespace AppBundle\EventListener;

use AppBundle\Document\Wall;
use AppBundle\Event\WallEvent;
use Doctrine\ODM\MongoDB\Event\PostFlushEventArgs;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ODM\MongoDB\Event\PreUpdateEventArgs;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;
use Eo\JobQueueBundle\Document\Job;

/**
 * @DI\Service
 * @DI\Tag("doctrine_mongodb.odm.event_listener", attributes = {"event" = "preUpdate"})
 * @DI\Tag("doctrine_mongodb.odm.event_listener", attributes = {"event" = "postFlush"})
 * @DI\Tag("kernel.event_subscriber")
 */
class WallEventlistener implements EventSubscriberInterface
{
    private $request;

    /**
     * @DI\InjectParams({
     *  "requestStack" = @DI\Inject("request_stack")
     * })
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getMasterRequest();
    }

    public static function getSubscribedEvents()
    {
        return array(
            WallEvent::CREATE => 'onWallCreate',
            WallEvent::DELETE => 'onWallDelete',
        );
    }

    public function onWallCreate($event)
    {
        if (!$this->request instanceof Request) {
            // don't do anything if it's not a request
            return;
        }
        $this->addRemovePosts($event->getRecord(), $event->dm);
    }

    public function onWallDelete($event)
    {
        if (!$this->request instanceof Request) {
            // don't do anything if it's not a request
            return;
        }

        $wall = $event->getRecord();
        $posts = $event->dm->getRepository('AppBundle:Post')->findByWallId($wall->getId());
        foreach ($posts as $post) {
            $event->dm->remove($post);
        }
    }

    public function preUpdate(PreUpdateEventArgs $event)
    {
        if ($event->getObject() instanceof Wall) {
            $wall = $event->getObject();
            $dm = $event->getDocumentManager();

            //On delete just return
            if ($event->hasChangedField('deletedAt')) {
                return;
            }

            //We can not use the hasChangedField because we receive it as string ("0" or "1") but we save it as boolean
            $allFacebookPostsHasChanged = $event->getOldValue('allFacebookPosts') != $event->getNewValue('allFacebookPosts');

            if ($allFacebookPostsHasChanged) {
                if ($event->getNewValue('allFacebookPosts')) {
                    $job = new Job('trick17.social_wall:facebook', array($wall->getId(), null, true));
                    $job->setMaxRetries(3);
                    $dm->persist($job);
                } else {
                    $postHashtags = $dm->getRepository('AppBundle:Post')->findHashtags($wall->getId());
                    $dm->getRepository('AppBundle:Post')->removeByNotHashtags($wall->getId(), $postHashtags);
                }
            }

            if ($event->hasChangedField('hashtags') || $event->hasChangedField('mainHashtag')) {
                $this->addRemovePosts($wall, $dm);
            }
        }
    }

    public function postFlush(PostFlushEventArgs $event)
    {
        $event->getDocumentManager()->flush();
    }

    private function addRemovePosts($wall, $dm)
    {
        $hashtags = $wall->getAllHashtags();
        $postHashtags = $dm->getRepository('AppBundle:Post')->findHashtags($wall->getId());

        $removeHashtags = array();
        $newHashtags = $hashtags;
        foreach ($postHashtags as $hashtag) {
            if (!in_array($hashtag, $hashtags)) {
                $removeHashtags[] = $hashtag;
            } else {
                if (($hashtag = array_search($hashtag, $newHashtags)) !== false) {
                    unset($newHashtags[$hashtag]);
                }
            }
        }

        $posts = $dm->getRepository('AppBundle:Post')->findByHashtags($wall->getId(), $removeHashtags);
        foreach ($posts as $post) {
            if (count($post->getHashtags()) > 1) {
                foreach ($removeHashtags as $removeHashtag) {
                    $post->removeHashtag($removeHashtag);
                }
                $dm->persist($post);
            }

            //Check count of hashtags again because it may changed and persisted in the if above
            if (count($post->getHashtags()) <= 1) {
                if ($post->getChannel() === 'facebook' && $wall->getAllFacebookPosts()) {
                    $post->removeHashtag(current($post->getHashtags()));
                    $dm->persist($post);
                } else {
                    $dm->remove($post);
                }
            }
        }

        if (count($newHashtags) > 1) {
            $job = new Job('trick17.social_wall:import_all', array($wall->getId()));
            $job->setMaxRetries(3);
            $dm->persist($job);
        } elseif (count($newHashtags) === 1) {
            $job = new Job('trick17.social_wall:import_all', array($wall->getId(), current($newHashtags)));
            $job->setMaxRetries(3);
            $dm->persist($job);
        }
    }
}
