<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidLanguageArrayKeys extends Constraint
{
    public $mustBeArrayMessage = 'Must be an array.';
    public $notValidKeysMessage = 'Array contains not valid language keys.';
}
