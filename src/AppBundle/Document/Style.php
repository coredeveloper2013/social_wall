<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Document\Repository\StyleRepository")
 *
 * @Gedmo\Loggable
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Style
{
    /**
     * @var int
     *
     * @MongoDB\Id
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $id;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $globalTextColor;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $primaryColor;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $primaryBackgroundColor;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $buttonColor;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $buttonBackgroundColor;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Versioned
     *
     * @Serializer\Expose
     * @Serializer\Groups({"default", "backend"})
     */
    private $extra;

    /**
     * @var string
     *
     * @MongoDB\String
     * @MongoDB\Index
     *
     * @Assert\NotBlank()
     */
    private $appInstance;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    private $createdAt;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Gedmo\Timestampable
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Blameable(on="create")
     */
    private $createdBy;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Gedmo\Blameable
     */
    private $updatedBy;

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set globalTextColor.
     *
     * @param string $globalTextColor
     *
     * @return self
     */
    public function setGlobalTextColor($globalTextColor)
    {
        $this->globalTextColor = $globalTextColor;

        return $this;
    }

    /**
     * Get globalTextColor.
     *
     * @return string $globalTextColor
     */
    public function getGlobalTextColor()
    {
        return $this->globalTextColor;
    }

    /**
     * Set primaryColor.
     *
     * @param string $primaryColor
     *
     * @return self
     */
    public function setPrimaryColor($primaryColor)
    {
        $this->primaryColor = $primaryColor;

        return $this;
    }

    /**
     * Get primaryColor.
     *
     * @return string $primaryColor
     */
    public function getPrimaryColor()
    {
        return $this->primaryColor;
    }

    /**
     * Set primaryBackgroundColor.
     *
     * @param string $primaryBackgroundColor
     *
     * @return self
     */
    public function setPrimaryBackgroundColor($primaryBackgroundColor)
    {
        $this->primaryBackgroundColor = $primaryBackgroundColor;

        return $this;
    }

    /**
     * Get primaryBackgroundColor.
     *
     * @return string $primaryBackgroundColor
     */
    public function getPrimaryBackgroundColor()
    {
        return $this->primaryBackgroundColor;
    }

    /**
     * Set buttonColor.
     *
     * @param string $buttonColor
     *
     * @return self
     */
    public function setButtonColor($buttonColor)
    {
        $this->buttonColor = $buttonColor;

        return $this;
    }

    /**
     * Get buttonColor.
     *
     * @return string $buttonColor
     */
    public function getButtonColor()
    {
        return $this->buttonColor;
    }

    /**
     * Set buttonBackgroundColor.
     *
     * @param string $buttonBackgroundColor
     *
     * @return self
     */
    public function setButtonBackgroundColor($buttonBackgroundColor)
    {
        $this->buttonBackgroundColor = $buttonBackgroundColor;

        return $this;
    }

    /**
     * Get buttonBackgroundColor.
     *
     * @return string $buttonBackgroundColor
     */
    public function getButtonBackgroundColor()
    {
        return $this->buttonBackgroundColor;
    }

    /**
     * Set extra.
     *
     * @param string $extra
     *
     * @return self
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra.
     *
     * @return string $extra
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set appInstance.
     *
     * @param string $appInstance
     *
     * @return self
     */
    public function setAppInstance($appInstance)
    {
        $this->appInstance = $appInstance;

        return $this;
    }

    /**
     * Get appInstance.
     *
     * @return string $appInstance
     */
    public function getAppInstance()
    {
        return $this->appInstance;
    }

    /**
     * Set createdAt.
     *
     * @param date $createdAt
     *
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param date $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return date $updatedAt
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy.
     *
     * @param string $createdBy
     *
     * @return self
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return string $createdBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param string $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return string $updatedBy
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}
