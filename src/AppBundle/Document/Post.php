<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Trick17\ApiBundle\Model\BaseObject;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="AppBundle\Document\Repository\PostRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Post extends BaseObject
{
    /**
     * @var int
     *
     * @MongoDB\Id
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @MongoDB\Index
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"public"})
     */
    private $postId;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $channel;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $text;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $link;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $userName;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $userLink;

    /**
     * @var string
     *
     * @MongoDB\String
     *
     * @Assert\NotBlank()
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     */
    private $userProfileImageUrl;

    /**
     * @var string
     *
     * @MongoDB\Collection
     *
     * @Assert\NotBlank()
     */
    private $hashtags;

    /**
     * @var bool
     *
     * @MongoDB\Boolean
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend"})
     */
    private $disabled;

    /**
     * @var Wall
     *
     * @MongoDB\ReferenceOne(targetDocument="Wall")
     * @MongoDB\Index
     *
     * @Assert\NotBlank()
     */
    private $wall;

     /**
      * @var string
      *
      * @MongoDB\String
      * @MongoDB\Index
      *
      * @Assert\NotBlank()
      */
     private $appInstance;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Serializer\Expose
     * @Serializer\Groups({"backend", "public"})
     * @Serializer\Type("DateTime<'Y-m-d\TH:i:sP'>")
     */
    private $createdAt;

    /**
     * @var
     *
     * @MongoDB\Date
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $importedAt;

    /**
     * Get id.
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postId.
     *
     * @param string $postId
     *
     * @return $this
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;

        return $this;
    }

    /**
     * Get postId.
     *
     * @return string $postId
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Set channel.
     *
     * @param string $channel
     *
     * @return $this
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel.
     *
     * @return string $channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set link.
     *
     * @param string $link
     *
     * @return $this
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set imageUrl.
     *
     * @param string $imageUrl
     *
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl.
     *
     * @return string $imageUrl
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set userName.
     *
     * @param string $userName
     *
     * @return $this
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName.
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userLink.
     *
     * @param string $userLink
     *
     * @return $this
     */
    public function setUserLink($userLink)
    {
        $this->userLink = $userLink;

        return $this;
    }

    /**
     * Get userLink.
     *
     * @return string $userLink
     */
    public function getUserLink()
    {
        return $this->userLink;
    }

    /**
     * Set userProfileImageUrl.
     *
     * @param string $userProfileImageUrl
     *
     * @return $this
     */
    public function setUserProfileImageUrl($userProfileImageUrl)
    {
        $this->userProfileImageUrl = $userProfileImageUrl;

        return $this;
    }

    /**
     * Get userProfileImageUrl.
     *
     * @return string $userProfileImageUrl
     */
    public function getUserProfileImageUrl()
    {
        return $this->userProfileImageUrl;
    }

    /**
     * Set hashtags.
     *
     * @param collection $hashtags
     *
     * @return $this
     */
    public function setHashtags($hashtags)
    {
        $this->hashtags = $hashtags;

        return $this;
    }

    /**
     * Get hashtags.
     *
     * @return collection $hashtags
     */
    public function getHashtags()
    {
        return $this->hashtags;
    }

    /**
     * Set disabled.
     *
     * @param bool $disabled
     *
     * @return $this
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled.
     *
     * @return bool $disabled
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set wall.
     *
     * @param AppBundle\Document\Wall $wall
     *
     * @return $this
     */
    public function setWall(\AppBundle\Document\Wall $wall)
    {
        $this->wall = $wall;

        return $this;
    }

    /**
     * Get wall.
     *
     * @return AppBundle\Document\Wall $wall
     */
    public function getWall()
    {
        return $this->wall;
    }

    /**
     * Set appInstance.
     *
     * @param string $appInstance
     *
     * @return $this
     */
    public function setAppInstance($appInstance)
    {
        $this->appInstance = $appInstance;

        return $this;
    }

    /**
     * Get appInstance.
     *
     * @return string $appInstance
     */
    public function getAppInstance()
    {
        return $this->appInstance;
    }

    /**
     * Set createdAt.
     *
     * @param date $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return date $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set importedAt.
     *
     * @param date $importedAt
     *
     * @return $this
     */
    public function setImportedAt($importedAt)
    {
        $this->importedAt = $importedAt;

        return $this;
    }

    /**
     * Get importedAt.
     *
     * @return date $importedAt
     */
    public function getImportedAt()
    {
        return $this->importedAt;
    }

    //Custom

    public function addHashtag($hashtag)
    {
        $this->hashtags[] = $hashtag;
    }

    public function removeHashtag($hashtag)
    {
        if (($key = array_search($hashtag, $this->hashtags)) !== false) {
            unset($this->hashtags[$key]);
        }
    }
}
