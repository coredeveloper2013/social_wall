<?php

namespace AppBundle\Document\Repository;

use Trick17\ApiBundle\Model\DocumentRepository;
use Trick17\ApiBundle\Exception\Error;
use Trick17\ApiBundle\Exception\ErrorException;
use Trick17\OAuthClientBundle\Security\OAuthUser;

class PostRepository extends DocumentRepository
{
    protected function findRecordsQuery($appInstance, $user, $request)
    {
        if (!isset($request['attributes']['filters']) && !isset($request['attributes']['filters']['wall.id'])) {
            $apiError = new Error(Error::ERROR_CODE_PARAMETERS_NOT_ALLOWED);
            throw new ErrorException($apiError);
        }
        $query = $this->createQueryBuilder();

        if (!is_object($user) || !$user instanceof OAuthUser) {
            $query->field('disabled')->notEqual(true);
        }

        return $query;
    }

    public function findPost($postId, $channel, $wall)
    {
        $query = $this->createQueryBuilder()
            ->field('postId')->equals((string) $postId)
            ->field('channel')->equals($channel)
            ->field('wall.id')->equals($wall->getId());

        return $query->getQuery()->execute()->getSingleResult();
    }

    public function findHashtags($wallId)
    {
        $query = $this->createQueryBuilder()
            ->distinct('hashtags')
            ->field('wall.id')->equals($wallId);

        return $query->getQuery()->execute()->toArray();
    }

    public function findByHashtags($wallId, array $hashtags)
    {
        $query = $this->createQueryBuilder()
            ->field('wall.id')->equals($wallId)
            ->field('hashtags')->in($hashtags);

        return $query->getQuery()->execute();
    }

    public function removeByNotHashtags($wallId, array $hashtags)
    {
        $this->createQueryBuilder()
            ->remove()
            ->field('wall.id')->equals($wallId)
            ->field('hashtags')->notIn($hashtags)
            ->getQuery()->execute();
    }

    public function findByWallId($wallId)
    {
        $query = $this->createQueryBuilder()
            ->field('wall.id')->equals($wallId);

        return $query->getQuery()->execute();
    }

    public function getOlderThen($wallId)
    {
        $query = $this->createQueryBuilder()
            ->select('createdAt')
            ->field('wall.id')->equals($wallId)
            ->sort('createdAt', 'DESC')
            ->skip(99)
            ->limit(1);

        $result = $query->getQuery()->execute()->getSingleResult();

        if ($result) {
            return $result->getCreatedAt();
        }
    }

    public function deletePostsOlderThen($wallId, $olderThen)
    {
        return $this->createQueryBuilder()
            ->remove()
            ->field('wall.id')->equals($wallId)
            ->field('createdAt')->lt($olderThen)
            ->getQuery()
            ->execute();
    }
}
