<?php

namespace AppBundle\Social\Status;

class Status
{
    public function getStatusResponse()
    {
        $status = $this->getStatus();

        $response['code'] = 0;
        $response['message'] = 'Nicht verbunden';
        if ($status === true) {
            $response['code'] = 1;
            $response['message'] = 'Verbunden';
        }

        $name = $this->getName();
        if (!empty($name)) {
            $response['name'] = $name;
        }
        $picture = $this->getPicture();
        if (!empty($picture)) {
            $response['picture'] = $picture;
        }
        $link = $this->getLink();
        if (!empty($link)) {
            $response['link'] = $link;
        }

        return $response;
    }
}
