<?php

namespace AppBundle\Social\Status;

use Facebook\Entities\AccessToken;
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("social.status.facebook")
 */
class FacebookStatus extends Status
{
    private $appInstance;

    /**
     * @DI\InjectParams({
     *      "settings" = @DI\Inject("settings"),
     *      "container" = @DI\Inject("service_container")
     * })
     */
    public function __construct($settings, $container)
    {
        $this->settings = $settings;
        $this->container = $container;
    }

    public function init($options)
    {
        if (isset($options['appInstance'])) {
            $this->settings->init($options);
        }
    }

    public function updateAccessToken($accessToken)
    {
        $appId = $this->container->getParameter('facebook_app_id');
        $appSecret = $this->container->getParameter('facebook_app_secret');

        FacebookSession::setDefaultApplication($appId, $appSecret);
        $session = new FacebookSession($accessToken);

        $info = (
            new FacebookRequest(
                $session,
                'GET',
                '/me',
                null,
                'v2.6'
            )
        )->execute()->getGraphObject();

        $this->settings->set('facebook-page', $info->getProperty('id'));
        $this->settings->set('facebook-name', $info->getProperty('name'));
        $this->settings->set('facebook-picture', 'https://graph.facebook.com/'.$info->getProperty('id').'/picture');
        $this->settings->set('facebook-link',  $info->getProperty('link'));

        $accessTokenObject = new AccessToken($accessToken);
        $longLivedAccessToken = $accessTokenObject->extend();
        $this->settings->set('facebook-access-token', $longLivedAccessToken->__toString());
    }

    public function getStatus()
    {
        $appId = $this->container->getParameter('facebook_app_id');
        $appSecret = $this->container->getParameter('facebook_app_secret');
        $accessToken = $this->settings->get('facebook-access-token');
        $page = $this->settings->get('facebook-page');

        if (empty($page)) {
            return false;
        }

        FacebookSession::setDefaultApplication($appId, $appSecret);
        $session = new FacebookSession($accessToken);

        try {
            $response = (
                new FacebookRequest(
                    $session,
                    'GET',
                    '/me',
                    null,
                    'v2.6'
                )
            )->execute()->getGraphObject();
        } catch (\Exception $e) {
            return false;
        }
        if (is_object($response) && get_class($response) === 'Facebook\GraphObject') {
            return $response->getProperty('id') === $page;
        }

        return false;
    }

    public function getStatusResponse()
    {
        $status = $this->getStatus();
        $page = $this->settings->get('facebook-page');

        $response = parent::getStatusResponse();
        if ($status === false) {
            if (empty($page)) {
                $response['code'] = 0;
                $response['message'] = 'Nicht konfiguriert';
            }
        }
        $response['type'] = 'facebook';
        if (!isset($response['name'])) {
            $response['name'] = 'Facebook';
        }

        return $response;
    }

    public function getName()
    {
        return $this->settings->get('facebook-name');
    }

    public function getPicture()
    {
        return $this->settings->get('facebook-picture');
    }

    public function getLink()
    {
        return $this->settings->get('facebook-link');
    }
}
