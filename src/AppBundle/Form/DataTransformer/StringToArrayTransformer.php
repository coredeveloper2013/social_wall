<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToArrayTransformer implements DataTransformerInterface
{
    public function transform($string)
    {
        return $string;
    }

    public function reverseTransform($array)
    {
        if (empty($array)) {
            return '';
        }

        return implode(',', $array);
    }
}
