<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateStylesCommand extends Command
{
    protected $container;

    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:generate-styles')
            ->setDescription('Generate Styles');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('doctrine_mongodb.odm.document_manager');
        $styleGenerator = $container->get('app_bundle.style.style_generator');

        $styles = $dm->getRepository('AppBundle\Document\Style')->findAll();
        foreach ($styles as $style) {
            $styleGenerator->generateWidgetStyle($style);
        }

        $output->writeln('Generated all styles');
    }
}
