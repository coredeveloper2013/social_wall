<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Abraham\TwitterOAuth\TwitterOAuth;
use AppBundle\Document\Post;

class TwitterCommand extends Command
{
    const CHANNEL = 'twitter';

    protected $container;

    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:twitter')
            ->setDescription('Twitter import')
            ->addArgument(
                'wall',
                InputArgument::OPTIONAL,
                'Id of wall'
            )
            ->addArgument(
                'hashtag',
                InputArgument::OPTIONAL,
                'Hashtag'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('t17_doctrine')->getManager();
        $exceptions = array();

        $twitter = new TwitterOAuth(
            $container->getParameter('twitter_consumer_key'),
            $container->getParameter('twitter_consumer_secret'),
            $container->getParameter('twitter_access_token'),
            $container->getParameter('twitter_access_token_secret')
        );

        $hashtags = $dm->getRepository('AppBundle:Wall')->findHashtags($input->getArgument('wall'), $input->getArgument('hashtag'));
        foreach ($hashtags as $hashtag => $walls) {
            try {
                $result = $twitter->get('search/tweets', array('q' => urlencode($hashtag)));
            } catch (\Exception $e) {
                $exceptions[] = $e->getMessage().' for '.$hashtag;
                continue;
            }
            if (isset($result->errors)) {
                $exceptions[] = current($result->errors)->message.' for '.$hashtag;
                continue;
            }
            foreach ($result->statuses as $tweet) {
                $postId = $tweet->id_str;
                foreach ($walls as $wall) {
                    $post = $dm->getRepository('AppBundle:Post')->findPost($postId, self::CHANNEL, $wall);
                    if (!$post) {
                        $post = new Post();
                        $post->setPostId($postId);
                        $post->setChannel(self::CHANNEL);
                        $post->setWall($wall);
                        $post->setAppInstance($wall->getAppInstance());

                        $post->setText($tweet->text);
                        $post->setLink('https://twitter.com/'.$tweet->user->screen_name.'/status/'.$postId);

                        $createdAt = date_create($tweet->created_at);
                        $createdAt->setTimezone(new \DateTimeZone('Europe/Rome'));
                        $post->setCreatedAt($createdAt);
                    }

                    if (!$post->getHashtags() || !in_array($hashtag, $post->getHashtags())) {
                        $post->addHashtag($hashtag);
                    }

                    if (isset($tweet->entities->media) && isset($tweet->entities->media[0])) {
                        $post->setImageUrl($tweet->entities->media[0]->media_url_https);
                    }
                    $post->setUserName($tweet->user->name);
                    $post->setUserProfileImageUrl($tweet->user->profile_image_url_https);
                    $post->setUserLink('https://twitter.com/'.$tweet->user->screen_name);

                    $dm->persist($post);
                }
                $dm->flush();
            }
        }

        if (!empty($exceptions)) {
            throw new \Exception(implode(', ', $exceptions));
        }
    }
}
