<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeletePostsCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('trick17.social_wall:delete-posts')
            ->setDescription('Keep only 100 posts per wall');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $dm = $container->get('doctrine_mongodb.odm.document_manager');

        $appInstances = $dm->getRepository('AppBundle:Wall')->getAppInstances();

        foreach ($appInstances as $appInstance) {
            $walls = $dm->getRepository('AppBundle:Wall')->findRecords($appInstance, null, null);
            foreach ($walls as $wall) {
                $olderThen = $dm->getRepository('AppBundle:Post')->getOlderThen($wall->getId());
                if ($olderThen) {
                    $deleteResponse = $dm->getRepository('AppBundle:Post')->deletePostsOlderThen($wall->getId(), $olderThen);
                    $output->writeln('Deleted '.$deleteResponse['n'].' for '.$appInstance.'.');
                }
            }
        }
    }
}
